variables:
  CI_BUILD_REF_NAME: $CI_COMMIT_REF_NAME
  CI_BUILD_ID: $CI_JOB_ID
  CI_BUILD_REPO: $CI_REPOSITORY_URL
  CI_BUILD_REF: $CI_COMMIT_SHA
  PYTHON: python3
  CONDA: http://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
  DEBIAN_FRONTEND: noninteractive
  DEBIAN_PACKAGE_MANAGERS: >
    cabal-install ghc
    cargo
    composer
    golang
    luarocks
    npm
    python3-pip
    r-base
    rubygems
    julia
    maven
  DEBIAN_OTHER_DEPS: >
    oracle-java8-installer
    curl
    perl-doc
    php-pear
  FEDORA_OTHER_DEPS: >
    lua-devel
    rubygem-rdoc
    curl
    bzip2
    perl-core
    php-pear
  # codecov depends on git, which is not default on openSUSE image
  # curl and unzip are needed by luarocks
  OPENSUSE_OTHER_DEPS: >
    curl
    git
    java-1_8_0-openjdk
    lua-devel
    unzip
    tar
    php5-pear

cache:
  paths:
    - ~/.local/share/coala-bears/
    - ~/.cache/pip
    - vendor/

.common_script: &common_script
  - pip3 install --upgrade pip setuptools
  - npm install -g bower
  - curl -fsSL -o miniconda.sh $CONDA
  - curl -sSL https://get.haskellstack.org/ | sh
  - stack setup
  - chmod +x miniconda.sh
  - ./miniconda.sh -b -p $HOME/miniconda
  - export PATH=$PATH:$HOME/miniconda/bin
  - export GOPATH=$HOME/go
  - go get -u github.com/gpmgo/gopm
  - go install github.com/gpmgo/gopm
  - mv $GOPATH/bin/gopm /usr/bin
  - cpan App::cpanminus
  - $PYTHON -m pip install -r requirements.txt -r test-requirements.txt
  - $PYTHON -m pytest --cov-fail-under=100
  - codecov

test:ubuntu_rolling:
  image: ubuntu:rolling
  variables:
    PYTHON: python3.6
  before_script:
    - apt-get update -qq
    - apt-get install -y -qq software-properties-common
    - echo 'oracle-java8-installer shared/accepted-oracle-license-v1-1
            select true' | /usr/bin/debconf-set-selections
    - add-apt-repository -y ppa:webupd8team/java
    - apt-get update -qq
    - apt-get install -y $DEBIAN_PACKAGE_MANAGERS $DEBIAN_OTHER_DEPS
  script: *common_script

test:fedora_25:
  image: fedora:25
  variables:
    PYTHON: python3.5
    PACKAGE_MANAGERS: $DEBIAN_PACKAGE_MANAGERS
  before_script:
    - echo 'install_weak_deps=False' >> /etc/dnf/dnf.conf
    - PACKAGE_MANAGERS=${PACKAGE_MANAGERS/r-base/R-base}
    # Installing Java 7 for checkstyle 6.19 is problematic
    - PACKAGE_MANAGERS=${PACKAGE_MANAGERS/maven/}
    - rm dependency_management/requirements/MavenRequirement.py
         tests/MavenRequirementTest.py
    - dnf install -y $PACKAGE_MANAGERS $FEDORA_OTHER_DEPS
  script: *common_script

test:opensuse_leap:
  image: opensuse:leap
  variables:
    PYTHON: python3.4
    PACKAGE_MANAGERS: $DEBIAN_PACKAGE_MANAGERS
    REPOS_BASE: http://download.opensuse.org/repositories
    RELEASE: openSUSE_Leap_42.2
    LUAROCKS_REPO: devel:languages:lua
    CARGO_REPO: home:Ledest:devel
  before_script:
    - zypper removerepo 'NON-OSS' 'Update Non-Oss'
    - PACKAGE_MANAGERS=${PACKAGE_MANAGERS/r-base/R-base}
    - PACKAGE_MANAGERS=${PACKAGE_MANAGERS/golang/go}
    - PACKAGE_MANAGERS=${PACKAGE_MANAGERS/composer/php-composer}
    - PACKAGE_MANAGERS=${PACKAGE_MANAGERS/luarocks/lua53-luarocks}
    # cargo installed separately below
    - PACKAGE_MANAGERS=${PACKAGE_MANAGERS/cargo/}
    # Installing Java 7 for checkstyle 6.19 is problematic
    - PACKAGE_MANAGERS=${PACKAGE_MANAGERS/maven/}
    - rm dependency_management/requirements/MavenRequirement.py
         tests/MavenRequirementTest.py
    # luarocks
    - zypper addrepo $REPOS_BASE/$LUAROCKS_REPO/$RELEASE/$LUAROCKS_REPO.repo
    - zypper --no-gpg-checks --non-interactive install
             $PACKAGE_MANAGERS $OPENSUSE_OTHER_DEPS
    # cargo
    - zypper addrepo $REPOS_BASE/$CARGO_REPO/$RELEASE/$CARGO_REPO.repo
    - zypper --no-gpg-checks --non-interactive install cargo rust-std
    # go list requires /usr/lib64/go/src/runtime/internal/sys/zversion.go
    - mkdir -p /usr/lib64/go/src/runtime/internal/sys/
    - touch /usr/lib64/go/src/runtime/internal/sys/zversion.go
  script: *common_script

check_code:
  image: ubuntu:rolling
  before_script:
    - apt-get update -qq
    - apt-get install -y -qq python3-pip git
    - pip3 install .
    - pip3 install coala-bears
  script:
    - coala-ci
